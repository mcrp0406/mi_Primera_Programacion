﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Programacion1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}


        async private void ValidateUser(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryUser.Text)|| string.IsNullOrEmpty(entryPassword.Text))
            {
                labelMessage.TextColor = Color.Red;
                labelMessage.Text = "Debe Escribir Usuario y Contraseña.";

                await DisplayAlert("Error", "Debe Escribir Usuario y Contraseña.", "Ok");
            
            }
            else 
            {
                labelMessage.TextColor = Color.Green;
                labelMessage.Text = "Su sesion fue Iniciada Correctamente";
                await Navigation.PushAsync(new Page2());
            }
        }

       
    }
}
