﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Programacion1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}

        async private void date(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryName.Text) || string.IsNullOrEmpty(entryLastName.Text) || string.IsNullOrEmpty(entryPhone.Text))
            {
                
                await DisplayAlert("Error", "Debe Escribir todos los Campos.", "Ok");

            }
            else
            {
                labelName.Text = entryName.Text;
                labelLastName.Text = entryLastName.Text;
                labelPhone.Text = entryPhone.Text;
            }
        }
    }
}